# Q-Frontend

React Frontend für das gesamte System.

## Aufbau

Dieses Projekt verwendet [Create React App](https://github.com/facebook/create-react-app), die verfügbaren Scripte sind nachfolgend zu finden.

Strukturiert ist das Projekt wie folgend beschrieben.

### Ordnerstruktur

Der gesamte React-Source Code befindet sich im `src`-Ordner. Der `app`-Ordner enthält die globalen Seiten und die zentrale `App.js`-Datei, die den zentralen Einstiegspunkt darstellt und auch das Routing an nachgeordnete Komponenten übernimmt.  
Diese befinden sich jeweils in eigenen  nach Service (oder Bereich) benannten Ordnern auf derselben Ebene. Innerhalb eines jeden Ordners befindet sich eine `.js`-Datei mit dem Namen des Services bzw. Bereiches, die das Routing an nachgeordnete Komponenten übernimmt. Im Ordner `pages` werden die Komponenten abgelegt, die eine Seite im Frontend darstellen. Teilkomponenten werden im Ordner `components` abgelegt. Bei der Benennung der Komponenten sollte CamelCase Schreibweise verwendet werden und der Service- bzw. Bereichname immer vorangestellt sein.

Der Order `shared` enthält Komponenten, die allgemein zur Verfügung stehen und von allen Komponenten verwendet werden können. Sie sind also nicht spezifisch für eine Komponente oder einen Service.

## Available Scripts from Create React App

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
