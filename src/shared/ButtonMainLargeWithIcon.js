/** @jsx jsx */
import { jsx } from "@emotion/core";
// import {Link} from 'react-router-dom';
import theme from '../app/theme';


function ButtonMainLargeWithIcon(props) {

  return (
    <div className='buttonContainer' >
      <button css={{
        minwitdh: '300px',
        minheight: '100px',
        paddingLeft: '15px',
        paddingRight: '15px',
        backgroundColor: theme.colors.primary,
        color: theme.colors.white,
        fontSize: '16px',
        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '5px',
        
        '&:hover' : {
          // backgroundColor: theme.colors.white,
          border: '3px 2px 2px rgba(0, 163, 71, 0.25)',
          // color: theme.colors.primary,
        }
      }} >
        <img src={props.buttonIcon} alt='icon' width='18' height='18' 
          css={{
            marginBottom: '3px',
            marginRight: '5px',

            '&:hover' : {
              color: 'black',
            }
          }} />
         {props.buttonText}
      </button>
    </div>
  );
}

export default ButtonMainLargeWithIcon;
