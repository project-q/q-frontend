/** @jsx jsx */
import { jsx } from "@emotion/core";

// Bootstrap
import './Styles.css'
import {Link} from 'react-router-dom';


function SiteHeaderLoginLogout(props) {

  if (window.localStorage.getItem('token')) {
    return (
      <a href="/" css={theme => ({
        paddingTop: "1px",
        float: "right",
        fontSize: "12px",
        color: theme.colors.white,
        ':hover,:focus': {
        color: theme.colors.graySemilight
        }
        })}
            onClick={() => {
              localStorage.removeItem('token');
              window.location.assign('/');
            }}
          >
            Abmelden
          </a>
    )
  } else {
    return (
      <Link to="/auth/login" css={theme => ({
        paddingTop: "1px",
        float: "right",
        fontSize: "12px",
        color: theme.colors.white,
        ':hover,:focus': {
        color: theme.colors.graySemilight
        }
        })}>Anmelden</Link>
    );
  }
}

export default SiteHeaderLoginLogout;
