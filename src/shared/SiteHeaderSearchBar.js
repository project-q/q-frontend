/** @jsx jsx */
import { jsx } from "@emotion/core";
import React from 'react';
import { Button, InputGroup, FormControl, Container } from 'react-bootstrap';
import searchIcon from './assets/search-icon.png';
import theme from "../app/theme";


class SiteHeaderSearchBar extends React.Component {
  constructor() {
    super();
    this.state = {
      search: '',
      isFiltered: false,
    };
  }

  handleSearch = (e) => {
    this.setState({search: e.target.value});
    this._handleSearch(e)
  }

  handleEnter = (e) => {
    if(e.keyCode === 13){
      this._handleSearch(e)
    }
  }

  _handleSearch = (e) => {
    this._changeFilteredValue();
    this.props.handleSearch(this.state.search, this.state.isFiltered);
  }

  _changeFilteredValue = (e) => {
    if(this.state.search.length === 0){
      this.state.isFiltered = false
    }else{
      this.state.isFiltered = true
    }
  }

  render(props) {
    // let filteredProduct = this.props.filteredProducts.filter(
    //   (product) => {
    //     return product.name.toLowerCase().indexOf(
    //       this.state.search) !== -1;
    //   }
    // );

    return (
      <Container>
        <InputGroup className="mb-3" style={{paddingTop: "13px"}}>
            <FormControl
              placeholder="Suche"
              aria-label="Suche"
              aria-describedby="basic-addon2"
              method="POST"
              value={this.state.search}
              onChange={this.handleSearch.bind(this)}
              onKeyUp={this.handleSearch.bind(this)}
              onKeyDown={this.handleEnter.bind(this)}
            />
            <InputGroup.Append>
              <Button variant="outline-secondary" size= 'sm' onClick={this._handleSearch.bind(this)} css={{
                backgroundColor: 'white',
                borderRadius: '0px 5px 5px 0px',
                borderColor: theme.colors.primary,
                height: '38px',
                color: 'white',
              }}>
                <img src={searchIcon} alt='search' width='18' />
              </Button>
          </InputGroup.Append>
        </InputGroup>
      </Container>
    );
  }
}

export default SiteHeaderSearchBar;
