/** @jsx jsx */
import logo from './assets/logo.png';
import { jsx } from "@emotion/core";

// Bootstrap
import theme from '../app/theme';
import './Styles.css'
import {Link} from 'react-router-dom';
import { Container, Navbar, Nav, Row, Col } from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";

// Components 
import SiteHeaderLoginLogout from './SiteHeaderLoginLogout';
import accountIcon from './assets/account-icon.png';
import dashboardIcon from './assets/dashboard-icon.png';
import shoppingCartIcon from './assets/shoppingcart-icon.png';
import SiteHeaderSearchBar from './SiteHeaderSearchBar.js';




function SiteHeader(props) {

  function handleSearchString(searchValue, isFiltered) {
    props.handleSearch(searchValue, isFiltered);
  }
  
  return (
    <Container fluid='true' style={{background: theme.colors.primary}}>

      <Row>
        <Col sm={{span: 2, offset: 10}}>
          <SiteHeaderLoginLogout />
        </Col>
      </Row>
        
      <Row>
        <Col>
      <Navbar collapseOnSelect sticky="top" expand="md" variant="dark">
        
        <LinkContainer to="/">
          <Navbar.Brand>
            <img src={logo} className="logo" alt="q_logo" height="100" width="100" />
          </Navbar.Brand>
        </LinkContainer>

        <Navbar.Toggle aria-controls="responsive-navbar-nav" />

        <Navbar.Collapse id="responsive-navbar-nav">
          <SiteHeaderSearchBar id="header-searchbar" handleSearch={handleSearchString.bind(this)} />

          <Nav className="mr-auto">
            <Nav.Item>
              <Nav.Link>
                <Link to="/dashboard">
                  <span className="menu--nav-link">
                    <img src={dashboardIcon} alt="account" width="50" height="50" />  
                    <span className="hide--on-sm" css={theme => ({ color: theme.colors.white })}>Dashboard</span>
                  </span>
                </Link>
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link>
                <Link to="/account">
                  <span className="menu--nav-link">
                    <img src={accountIcon} alt="account" width="50" height="50" />  
                    <span className="hide--on-sm" css={theme => ({ color: theme.colors.white })}>Account</span>
                  </span>
                </Link>
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link>
                <Link to="/shoppingcart">
                  <span className="menu--nav-link">
                    <img src={shoppingCartIcon} alt="shoppingCart" width="50" height="50" />  
                    <span className="hide--on-sm" css={theme => ({ color: theme.colors.white })}>Einkaufswagen</span>
                  </span>
                </Link>
              </Nav.Link>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>

      </Navbar>
      </Col>
      </Row>
    </Container>
  );
}

export default SiteHeader;
