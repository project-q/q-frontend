/** @jsx jsx */
import { jsx } from "@emotion/core";
// import {Link} from 'react-router-dom';
import theme from '../app/theme';


function ButtonMainSmall(props) {

  return (
    <div className='buttonContainer'>
      <button css={{
        minwitdh: '150px',
        maxheight: '30px',
        paddingLeft: '15px',
        paddingRight: '15px',
        backgroundColor: theme.colors.white,

        color: theme.colors.primary,
        fontSize: '12px',

        boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
        borderRadius: '5px',
        
        '&:hover' : {
          backgroundColor: theme.colors.primary,
          border: '3px 2px 2px rgba(0, 163, 71, 0.25)',
          color: theme.colors.white,
        }
      }} >
         {props.buttonText}
      </button>
    </div>
  );
}

export default ButtonMainSmall;
