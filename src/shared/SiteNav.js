import React from 'react';
import {Link} from 'react-router-dom';


function SiteNav() {
  return (
    <nav className="SiteNav">
      <ul>
        <li><Link to="/">Home</Link></li>
        <li><Link to="/auth/login">Login</Link></li>
        <li><Link to="/dashboard">Dashboard</Link></li>
      </ul>
    </nav>
  );
}

export default SiteNav;
