/** @jsx jsx */
import { jsx } from "@emotion/core";
// import theme from '../app/theme.js';
import {Link} from 'react-router-dom';
import { Button, Container } from "react-bootstrap";


function SiteHeaderNavigationButton(props) {

  // function handleClick(path) {
  //   console.log({path})
  //   // Route zu Page einfügen // onClick={() => handleClick(props.pathTo)}
  //   // TODO: Ändern! 
  // }

    return (
      <Container fluid="true">
        <Button 
          style={{
            padding: '1px',
            borderRadius: '30px',
            "&:hover": {
              transition:'.5s',
              background: '#008B47',
            }
          }}>
            <Link to={props.pathTo}>
              <img src={props.navIcon} className="logo" alt="nav" height="45" width="45" />  
            </Link>
        </Button>
      </Container>
    );
  }

export default SiteHeaderNavigationButton;
