/** @jsx jsx */
import { jsx } from "@emotion/core";
import SiteHeaderNavigationButton from './SiteHeaderNavigationButton';
import accountIcon from './assets/account-icon.png';
import dashboardIcon from './assets/dashboard-icon.png';
import shoppingCardIcon from './assets/shoppingcart-icon.png';
import { Container, Row, Col } from "react-bootstrap";


function SiteHeaderUserNavigation(props) {
  return (

    <Container fluid="true" >
      <Row>
        <Col md={4}>
          <SiteHeaderNavigationButton navIcon={dashboardIcon} pathTo={'/dashboard'} /> 
        </Col>
        <Col md={4}>
          <SiteHeaderNavigationButton navIcon={accountIcon} pathTo={'/account'} />
        </Col>
        <Col md={4}>
          <SiteHeaderNavigationButton navIcon={shoppingCardIcon} pathTo={'/shoppingcard'} />
        </Col>
      </Row>
    </Container>
  );
}

export default SiteHeaderUserNavigation;
