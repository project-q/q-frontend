/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Route } from 'react-router-dom';

import Shoppage from './pages/Shoppage';
import ProductPage from '../product/pages/Productpage';
import FilteredShopPageByCategory from "./pages/FilteredShopPageByCategory";

function Shop({ match }) {
  return (
    <div>
      <Route exact path={`${match.path}`} component={Shoppage} />
      <Route path={`${match.path}/to-product`} component={ProductPage} />
      <Route path={`${match.path}/category-cereal`} render={()=><FilteredShopPageByCategory category="CEREAL"/>} />
      <Route path={`${match.path}/category-cereal-products`} render={()=><FilteredShopPageByCategory category="CEREAL_PRODUCTS"/>} />
      <Route path={`${match.path}/category-potato`} render={()=><FilteredShopPageByCategory category="POTATO"/>} />
      <Route path={`${match.path}/category-potato-products`} render={()=><FilteredShopPageByCategory category="POTATO_PRODUCTS"/>} />
      <Route path={`${match.path}/category-fruit`} render={()=><FilteredShopPageByCategory category="FRUIT"/>} />
      <Route path={`${match.path}/category-fruit-products`} render={()=><FilteredShopPageByCategory category="FRUIT_PRODUCTS"/>} />
      <Route path={`${match.path}/category-dried-fruit`} render={()=><FilteredShopPageByCategory category="DRIED_FRUIT"/>} />
      <Route path={`${match.path}/category-vegetables`} render={()=><FilteredShopPageByCategory category="VEGETABLES"/>} />
      <Route path={`${match.path}/category-pulses`} render={()=><FilteredShopPageByCategory category="PULSES"/>} />
      <Route path={`${match.path}/category-nuts`} render={()=><FilteredShopPageByCategory category="NUTS"/>} />
      <Route path={`${match.path}/category-seeds`} render={()=><FilteredShopPageByCategory category="SEEDS"/>} />
      <Route path={`${match.path}/category-meat`} render={()=><FilteredShopPageByCategory category="MEAT"/>} />
      <Route path={`${match.path}/category-meat-products`} render={()=><FilteredShopPageByCategory category="MEAT_PRODUCTS"/>} />
      <Route path={`${match.path}/category-fish`} render={()=><FilteredShopPageByCategory category="FISH"/>} />
      <Route path={`${match.path}/category-fish-products`} render={()=><FilteredShopPageByCategory category="FISH_PRODUCTS"/>} />
      <Route path={`${match.path}/category-milk`} render={()=><FilteredShopPageByCategory category="MILK"/>} />
      <Route path={`${match.path}/category-dairy-products`} render={()=><FilteredShopPageByCategory category="DAIRY_PRODUCTS"/>} />
      <Route path={`${match.path}/category-egg`} render={()=><FilteredShopPageByCategory category="EGG"/>} />
      <Route path={`${match.path}/category-egg-dishes`} render={()=><FilteredShopPageByCategory category="EGG_DISHES"/>} />
      <Route path={`${match.path}/category-oils`} render={()=><FilteredShopPageByCategory category="OILS"/>} />
      <Route path={`${match.path}/category-fats`} render={()=><FilteredShopPageByCategory category="FATS"/>} />
      <Route path={`${match.path}/category-confectionery`} render={()=><FilteredShopPageByCategory category="CONFECTIONERY"/>} />
      <Route path={`${match.path}/category-sugar`} render={()=><FilteredShopPageByCategory category="SUGAR"/>} />
      <Route path={`${match.path}/category-preserves`} render={()=><FilteredShopPageByCategory category="PRESERVES"/>} />
      <Route path={`${match.path}/category-ready-meals`} render={()=><FilteredShopPageByCategory category="READY_MEALS"/>} />
      <Route path={`${match.path}/category-sauces`} render={()=><FilteredShopPageByCategory category="SAUCES"/>} />
      <Route path={`${match.path}/category-seasonings`} render={()=><FilteredShopPageByCategory category="SEASONINGS"/>} />
      <Route path={`${match.path}/category-spices`} render={()=><FilteredShopPageByCategory category="SPICES"/>} />
      <Route path={`${match.path}/category-herbs`} render={()=><FilteredShopPageByCategory category="HERBS"/>} />
      <Route path={`${match.path}/category-non-alcoholic`} render={()=><FilteredShopPageByCategory category="NON_ALCOHOLIC"/>} />
      <Route path={`${match.path}/category-alcoholic`} render={()=><FilteredShopPageByCategory category="ALCOHOLIC"/>} />
    </div>
  );
}

export default Shop;
