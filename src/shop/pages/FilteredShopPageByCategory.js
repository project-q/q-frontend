import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import ClipLoader from 'react-spinners/ClipLoader';

// Components
import ProductTile from '../components/ProductTile';
import FilterNavigation from '../components/FilterNavigation';
import { GET_PRODUCTSBYCATEGORYFILTER } from '../../shared/ProductService';

// Bootstrap
import { Container, Row, Col, CardDeck } from 'react-bootstrap';

function FilteredShopPageByCategory({category}){
  const params = {
    category
  }
  const { loading, error, data } = useQuery(GET_PRODUCTSBYCATEGORYFILTER, {
    variables: params,
  });
  if (loading) return <div class="text-center"><ClipLoader size={150} color={"#00A347"} loading={true}></ClipLoader></div>;
  if (error) return <p>{error.message}</p>;
  if (data) console.log(data);

  if (data) {
    return (
      <Container>

        <Row>
          <Col className="hidden" sm={3} md={3} lg={3}>
            <FilterNavigation />
          </Col>
          <Col sm={12} md={12} lg={9}>
            <CardDeck>
              {(data.getProductsByCategory.length === 0) ? (
                <Fallback />
              ) : (

                  data.getProductsByCategory.map(({ _id, name, images, producerID, offerTypes, rating }) => (
                    <ProductTile
                      key={_id}
                      id={_id}
                      images={images.map(({ name }) => (name))}
                      producer={producerID}
                      name={name}
                      price={offerTypes.map(({ price }) => (price))}
                      amount={offerTypes.map(({ amount }) => (amount))}
                      unit={offerTypes.map(({ unit }) => (unit))}
                      rating={rating}
                    />
                  ))

                )}
            </CardDeck>
          </Col>
        </Row>

      </Container>
    );
  } else {
    return <h2>Produkte können derzeit nicht abgefragt werden.</h2>
  }
}

function Fallback() {
  return (
    <h2 class="text-center">Keine Ergebnisse.</h2>
  );
}

export default FilteredShopPageByCategory;
