import React from 'react';
import {gql} from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

// Components
import ProductTile from '../components/ProductTile';
import FilterNavigation from '../components/FilterNavigation';

// Bootstrap
import { Container, Row, Col, CardDeck } from 'react-bootstrap';


const GET_ALLPRODUCTS = gql`
  query getAllProducts{
    getAllProducts{
      _id
      producerID
      name
      images {
        name
        resolutions
      }
      offerTypes {
        price
        amount
        unit
      }
      rating
    }
  }
`; 

function handleFilter(filter) {
  console.log(filter);
}

function Shoppage(props) {
  const {loading, error, data} = useQuery(GET_ALLPRODUCTS);
  if (loading) return <p>Loading..</p>;
  if (error) return <p>{error.message}</p>;
  if(data) {console.log("Data: " + data )};

  // console.log(data.getAllProducts.map(({offerTypes}) => (
  //   offerTypes.map(({price}) => (
  //     {price}
  //   ))
  // )));
  

  if (data) {
    return (
      <Container>
        {(data.length === 0) ? (
          <Fallback />
        ) : (
          <Row>
            <Col className="hidden" sm={3} md={3} lg={3}>
              <FilterNavigation handleFilter={handleFilter.bind(this)}/>
            </Col>
            <Col sm={12} md={12} lg={9}>
            <CardDeck>
            {data.getAllProducts.map(({_id, name, images, producerID, offerTypes, rating}) => (
              <ProductTile
                key={_id} 
                id={_id}
                images={images.map(({name}) => (name))}
                producer={producerID}
                name={name}
                price={offerTypes.map(({price}) => (price))}
                amount={offerTypes.map(({amount}) => (amount))}
                unit={offerTypes.map(({unit}) => (unit))}
                rating={rating}
                />
            ))}
            </CardDeck>
            </Col>
          </Row>
        )}
      </Container>  
    );
    } else {
      return <p>Keine Data vorhanden!</p>
    }
}

function Fallback() {
  return (
    <div className="fallback-section">
      <h3>Keine Produkte vorhanden</h3>
    </div>
  );
}

export default Shoppage;
