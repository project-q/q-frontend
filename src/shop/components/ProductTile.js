/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Link } from 'react-router-dom';
// import theme from '../../app/theme'

// Bootstrap
import { Card } from "react-bootstrap";

// Components
import RatingStars from "./RatingStars";


function ProductTile(props) {

  window.localStorage.removeItem('productId')

  /* 
  <Link to={{ pathname: '/shop/to-product, state: { productId: props.id}}}>
const { fromNotifications } = this.props.location.state

    <Link to='/shop/to-product' style={{ color: "black", textDecoration: 'none' }}>
  */

  return (
    <Link to={{ pathname: '/shop/to-product', state: { productId: props.id } }}>
      <Card css={{
        '&:hover': {
          backgroundColor: '#F5F5F5',
          border: '3px 2px 2px rgba(0, 163, 71, 0.25)',
        }
      }} style={{
        width: '13rem',
        margin: '5px',
        boxShadow: '0px 4px 4px rgba(0,0,0,0.25)',
      }}>
        <Card.Img variant="top" src={props.images} />
        <Card.Body>
          <Card.Title style={{ fontSize: "15px", letterSpacing: "1px" }}>{props.name}</Card.Title>
          {/* <Card.Text style={{ fontSize: "13px", lineHeight: "16px" }}>{props.producer}</Card.Text> */}
          <Card.Subtitle style={{ fontSize: "15px" }}>{props.price} € /  </Card.Subtitle>
          <Card.Text style={{ fontSize: "12px" }}> {props.amount} Stk./ {props.price} €</Card.Text>
          <RatingStars style={{ margin: "-2px", padding: "-2px" }} amount={props.rating} />
        </Card.Body>
      </Card>
    </Link>
  );
}


export default ProductTile;
