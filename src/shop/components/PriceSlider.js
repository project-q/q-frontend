/** @jsx jsx */
import { jsx } from "@emotion/core";
import { Container, Row } from "react-bootstrap";



function PriceSlider(props) {

  return (
    <Container>
      <Row>
        <form className="multi-range-field">
          <input id="multi" class="multi-range" type="range" style={{width: "100%"}} />
        </form>
      </Row>
    </Container>
      
  );
}

export default PriceSlider;
