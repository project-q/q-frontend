/** @jsx jsx */
import { jsx } from "@emotion/core";
import theme from '../../app/theme';

// Components
import PriceSlider from "./PriceSlider";
import RatingStars from "./RatingStars";
import { Link } from 'react-router-dom';
import { Container, Row, Col } from "react-bootstrap";

function FilterNavigation(props) {

  return (
    <Container style={{ width: "200px", backgroundColor: theme.colors.grayLight, padding: '11px', borderRadius: '30px', boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)', }}>
      <Row style={{ paddingLeft: '3px', }}>
        <Col>
          <h4><Link id="allCategories" to="/">Alle Kategorien</Link></h4>

          <ul class="list-group">
            <Link id="cereal" to="/shop/category-cereal" exact>Getreide</Link>
            <Link id="cereal-products" to="/shop/category-cereal-products" exact>Getreideprodukte</Link>
            <Link id="potato" to="/shop/category-potato" exact>Kartoffel</Link>
            <Link id="potato-products" to="/shop/category-potato-products" exact>Kartoffelprodukte</Link>
            <Link id="fruit" to="/shop/category-fruit" exact>Obst</Link>
            <Link id="fruit-products" to="/shop/category-fruit-products" exact>Obstprodukte</Link>
            <Link id="dried-fruit" to="/shop/category-dried-fruit" exact>Getrocknetes Obst</Link>
            <Link id="vegetables" to="/shop/category-vegetables" exact>Gemüse</Link>
            <Link id="pulses" to="/shop/category-pulses" exact>Hülsenfrüchte</Link>
            <Link id="nuts" to="/shop/category-nuts" exact>Nüsse</Link>
            <Link id="seeds" to="/shop/category-seeds" exact>Saatgut</Link>
            <Link id="meat" to="/shop/category-meat" exact>Fleisch</Link>
            <Link id="meat-products" to="/shop/category-meat-products" exact>Fleischprodukte</Link>
            <Link id="fish" to="/shop/category-fish" exact>Fisch</Link>
            <Link id="fish-products" to="/shop/category-fish-products" exact>Fischerei Produkte</Link>
            <Link id="milk" to="/shop/category-milk" exact>Milch</Link>
            <Link id="dairy-products" to="/shop/category-dairy-products" exact>Milchprodukte</Link>
            <Link id="egg" to="/shop/category-egg" exact>Eier</Link>
            <Link id="egg-dishes" to="/shop/category-egg-dishes" exact>Eiprodukte</Link>
            <Link id="oils" to="/shop/category-oils" exact>Öle</Link>
            <Link id="fats" to="/shop/category-fats" exact>Fette</Link>
            <Link id="confectionery" to="/shop/category-confectionery" exact>Süßwaren</Link>
            <Link id="sugar" to="/shop/category-sugar" exact>Zucker</Link>
            <Link id="preserves" to="/shop/category-preserves" exact>Konserven</Link>
            <Link id="ready-meals" to="/shop/category-ready-meals" exact>Fertiggerichte</Link>
            <Link id="sauces" to="/shop/category-sauces" exact>Soßen</Link>
            <Link id="seasonings" to="/shop/category-seasonings" exact>Gewürzmischungen</Link>
            <Link id="spices" to="/shop/category-spices" exact>Gewürze</Link>
            <Link id="herbs" to="/shop/category-herbs" exact>Kräuter</Link>
            <Link id="non-alcoholic" to="/shop/category-non-alcoholic" exact>Getränke</Link>
            <Link id="alcoholic" to="/shop/category-alcoholic" exact>Alkoholische Getränke</Link>
          </ul>

          <h4>Preis</h4>
          <PriceSlider />

          <h4>Bewertungen</h4>
          <RatingStars amount={5} />
          <RatingStars amount={4} />
          <RatingStars amount={3} />
          <RatingStars amount={2} />
          <RatingStars amount={1} />
        </Col>
      </Row>
    </Container>
  );
}

export default FilterNavigation;
