import React from 'react';
import { useState, useEffect } from 'react';
import Shoppage from '../shop/pages/Shoppage';
import FilteredShoppage from '../shop/pages/FilteredShopPage';
import { Jumbotron } from 'react-bootstrap';
// import { BrowserRouter as Router, Route } from 'react-router-dom';

/* const GET_AllTemplates = gql`
  {
    getAllTemplates {
      id
      title
    }
  }
`;

function Templates({ onTemplateSelected }) {
  const { loading, error, data } = useQuery(GET_AllTemplates);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <div>
      ProductTemplates:
      <select name="template" onChange={onTemplateSelected}>
        {data.getAllTemplates.map(template => (
          <option key={template.id} value={template.title}>
            {template.title}
          </option>
        ))}
      </select>
    </div>
  );
} */



function Home(props) {
  const [searchValue, setValue] = useState();
  const [isFiltered, setFiltered] = useState();

  useEffect(() => {
    setValue(props.searchVal);
    setFiltered(props.isFiltered);
  }, [props.searchVal, props.isFiltered])
  
  console.log("Home: " + searchValue)

  return (
    <div className="Home container main-content">
      
        {!isFiltered ? (
          <>
          <Jumbotron>
            <h1>Willkommen bei Projekt Q!</h1>
            <p>Wir vernetzen lokale Erzeuger und Verbraucher durch bestehende Logistikrouten und bieten Produzenten ein klimafreundliche Alternative ihre Reichweite zu erweitern.</p>
          </Jumbotron>
          <Shoppage />
          </>
        ) : (
          <FilteredShoppage searchVal={searchValue} />  
        )}
    </div>
  );
}

export default Home;
