// <div className="Productpage-container" style={{margin: '11px'}}>
// {/* TODO: Verfügbarkeit, Profilabschnitt mit Detail, Mehr Infos, Bewertungen? */}
//   <Container>
//     <Row>
//       <Col xs={6}>
//           {/* <ImageGallery items={images} showNav={false} showFullscreenButton={false} showPlayButton={false}/> */}
//       </Col>

//       <Col xs={6}>

//           <h3 style={{letterSpacing: "2px"}}>Bio-Erdbeeren </h3>
//           <RatingStars amount={5}/>
//           <p style={{fontSize: '16px'}}>Schlandbauerngut</p>

//           <p style={{fontSize: '14px'}}>
//           Beschreibung Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
//           sed diam nonumy eirmod tempor invidunt ut labore et dolore magna 
//           aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo 
//           dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus 
//           est Lorem ipsum dolor sit amet.
//           </p>
//           <p style={{fontSize: '12px'}}>inkl. 10% MwSt., zzgl. Versandkosten, </p>  
//           <p style={{fontSize: '14px'}}>29,99€ / 10kg</p> 
//           <h3>2,99 /kg</h3>

//         <Row>
//           <Col xs={3} ><ProductAmoutCounter/> </Col>
//           <div class="text-center">
//             <Link className="btn btn-primary" to="/product/add-to-cart">Zum Warenkorb hinzufügen</Link>
//          </div>
//         </Row>

//       </Col>

//     </Row>
//   </Container>


import React from 'react';
// import { useState } from 'react';
import { Link } from 'react-router-dom';

//Bootstrap
import { Container, Row, Col, Card, Tab, Tabs } from 'react-bootstrap';



function ProductBottom({ props }) {

  return (
    <div className="Productpage-container" style={{ margin: '11px' }}>

      <Container>
        <Row>
          <Col>
            <Tabs defaultActiveKey="details" >
              <Tab eventKey="details" title="Details">
                <p style={{ fontSize: '14px' }}>{`Beschreibung: ${props.longDescription}`}</p>
                <p style={{ fontSize: '14px' }}>{`Kategorie: ${props.category}`}</p>
                <p style={{ fontSize: '14px' }}>{`Allergien: ${props.allergen}`}</p>
                <p style={{ fontSize: '14px' }}>{`Farbe: ${props.color}`}</p>
                <p style={{ fontSize: '14px' }}>{`Verpackung: ${props.packaging}`}</p>
                <p style={{ fontSize: '14px' }}>{`Zustand: ${props.condition}`}</p>

              </Tab>
              <Tab eventKey="profile" title="Profil">
                <p style={{ fontSize: '14px' }}>{`Hier kommen später die öffentlichen Profildaten von ${props.producerID} hin`}</p>
              </Tab>
              <Tab eventKey="rating" title="Bewertungen" >
                <p style={{ fontSize: '14px' }}>zzz</p>
              </Tab>
            </Tabs>
          </Col>

          <Col>
            <Card>
              <Card.Header as="h5">Erzeuger</Card.Header>
              <Card.Body>
                <Card.Title>{props.producerID}</Card.Title>
                <Card.Text>
                  <p style={{ fontSize: '14px' }}>
                    Beschreibung Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                </p>
                </Card.Text>
                <Link className="btn btn-outline-secondary" to="/product/more-information">{`Mehr zu ${props.producerID}`}</Link>
              </Card.Body>
            </Card>
          </Col>

        </Row>
      </Container>
    </div>
  );
}

export default ProductBottom;
